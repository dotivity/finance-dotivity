<style type="text/css">
.sidebar .menu .list .ml-menu i.material-icons{
  font-size: 14px;
  margin-top: 1px;
  margin-right: 10px;
}
.bootstrap-select{
  border: 1px solid #ccc !important;
}
</style>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title><?php echo $title; ?></title>
  <!-- Favicon-->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/<?php echo favicon(); ?>" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

  <!-- Waves Effect Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

  <!-- Animation Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

  <!-- JQuery DataTable Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

  <!-- Morris Chart Css-->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/morrisjs/morris.css" rel="stylesheet" />

  <!-- Bootstrap Material Datetime Picker Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" />
  <!-- Wait Me Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/waitme/waitMe.css" rel="stylesheet" />

  <!-- Bootstrap Select Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

  <!-- Custom Css -->
  <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

  <!-- AdminBSB Themes. You can choose a theme from css/ themes instead of get all themes -->
  <link href="<?php echo base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-purple">
  <!-- Page Loader -->
  <div class="page-loader-wrapper">
    <div class="loader">
      <div class="preloader">
        <div class="spinner-layer pl-red">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
      <p>Please wait...</p>
    </div>
  </div>
  <!-- #END# Page Loader -->
  <!-- Overlay For Sidebars -->
  <div class="overlay"></div>
  <!-- #END# Overlay For Sidebars -->
  <!-- Search Bar -->
  <div class="search-bar">
    <div class="search-icon">
      <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
      <i class="material-icons">close</i>
    </div>
  </div>
  <!-- #END# Search Bar -->
  <!-- Top Bar -->
  <nav class="navbar bg-gradien">
    <?php include('top-navbar.php') ?>
  </nav>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <?php include('sidebar_menu.php') ?>
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <?php echo $contents; ?>
    </div>
  </section>

  <!-- Jquery Core Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery/jquery-3.3.1.min.js"></script>

  <!-- Bootstrap Core Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.min.js"></script>

  <!-- Select Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

  <!-- Slimscroll Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

  <!-- Waves Effect Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

  <!-- Jquery CountTo Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-countto/jquery.countTo.js"></script>

  <!-- Morris Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/raphael/raphael.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/morrisjs/morris.js"></script>

  <!-- ChartJs -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/chartjs/Chart.bundle.js"></script>

  <!-- Flot Charts Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.resize.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.pie.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.categories.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.time.js"></script>

  <!-- Sparkline Chart Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>

  <!-- Jquery DataTable Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

  <!-- Autosize Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/autosize/autosize.js"></script>

  <!-- Moment Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/momentjs/moment.js"></script>

  <!-- Bootstrap Material Datetime Picker Plugin Js -->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
  <script src=" https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script src=" https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

  <!-- Custom Js -->
  <script src="<?php echo base_url(); ?>assets/admin/js/admin.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/pages/index.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/pages/tables/jquery-datatable.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/pages/forms/basic-form-elements.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <!-- Demo Js -->
  <script src="<?php echo base_url(); ?>assets/admin/js/demo.js"></script>
  <script type="text/javascript">
  function jam(){
    var waktu = new Date();
    var jam = waktu.getHours();
    var menit = waktu.getMinutes();
    var detik = waktu.getSeconds();

    if (jam < 10){ jam = "0" + jam; }
    if (menit < 10){ menit = "0" + menit; }
    if (detik < 10){ detik = "0" + detik; }
    var jam_div = document.getElementById('jam');
    jam_div.innerHTML = jam + ":" + menit + ":" + detik;
    setTimeout("jam()", 1000);
  } jam();
  </script>

  <script>
  $(function(){
    $("#tampil").click(function(){
      var vtanggal = $("#vtanggal").val();
      $.ajax({
        url:"<?php echo site_url('administrator/tampil_data');?>",
        type:"POST",
        data:"vtanggal="+vtanggal,
        cache:false,
        success:function(html){
          $("#tampil_data").html(html);
        }
      })
    })

  })
  </script>

  <script>
  $('#date').bootstrapMaterialDatePicker({
    time: false,
    clearButton: true,
  });
  </script>

  <script>
  $('#date1').bootstrapMaterialDatePicker({
    time: false,
    clearButton: true,
  });
  </script>



  <script type="text/javascript">
  //fungsi untuk filtering data berdasarkan tanggal
  var start_date;
  var end_date;
  var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
    var dateStart = parseDateValue(start_date);
    var dateEnd = parseDateValue(end_date);
    //Kolom tanggal yang akan kita gunakan berada dalam urutan 2, karena dihitung mulai dari 0
    //nama depan = 0
    //nama belakang = 1
    //tanggal terdaftar =2
    var evalDate= parseDateValue(aData[1]);
    if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
    ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
    ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
    ( dateStart <= evalDate && evalDate <= dateEnd ) )
    {
      return true;
    }
    return false;
  });

  // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
  function parseDateValue(rawDate) {
    var dateArray= rawDate.split("-");
    var parsedDate= new Date(dateArray[0], parseInt(dateArray[1])-1, dateArray[2]);  // -1 because months are from 0 to 11
    return parsedDate;
  }

  $( document ).ready(function() {
    var numberRenderer = $.fn.dataTable.render.number( ',', '.', ).display;
    //konfigurasi DataTable pada tabel dengan id example dan menambahkan  div class dateseacrhbox dengan dom untuk meletakkan inputan daterangepicker
    var $dTable = $('#example').DataTable({
      "dom": "Bfrtip" +
      "<'row'<'col-sm-4'><'col-sm-5' <'datesearchbox'>><'col-sm-3'>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-5'i><'col-sm-7'p>>" ,
      "paging": false,
      "autoWidth": true,
      "buttons": ["print"],
      "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
          return typeof i === 'string' ?
          i.replace(/[^\d]/g, '')*1 :
          typeof i === 'number' ?
          i : 0;
        };

        // Total over all pages
        total = api
        .column( 5 )
        .data()
        .reduce( function (a, b) {
          return intVal(a) + intVal(b);
        }, 0 );

        // Total over this page
        pageTotal = api
        .column( 5, { page: 'current'} )
        .data()
        .reduce( function (a, b) {
          return intVal(a) + intVal(b);
        }, 0 );


        // Total filtered rows on the selected column (code part added)
        var sumCol4Filtered = display.map(el => data[el][5]).reduce((a, b) => intVal(a) + intVal(b), 0 );

        // Update footer
        $( api.column( 5 ).footer() ).html(
          'Rp. '+numberRenderer( pageTotal )
        );
      }
    });

    //menambahkan daterangepicker di dalam datatables
    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range.."> </div>');

    document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";

    //konfigurasi daterangepicker pada input dengan id datesearch
    $('#datesearch').daterangepicker({
      autoUpdateInput: false,
      "autoApply": true,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      "alwaysShowCalendars": true,
      locale: {
        cancelLabel: 'Clear',}
      });

      //menangani proses saat apply date range
      $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
        start_date=picker.startDate.format('YYYY-MM-DD');
        end_date=picker.endDate.format('YYYY-MM-DD');
        $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
        $dTable.draw();
      });

      $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        start_date='';
        end_date='';

        $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
        $dTable.draw();
      });



    });
    </script>

    <script type="text/javascript">
    //fungsi untuk filtering data berdasarkan tanggal
    var start_date;
    var end_date;
    var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
      var dateStart = parseDateValue(start_date);
      var dateEnd = parseDateValue(end_date);
      //Kolom tanggal yang akan kita gunakan berada dalam urutan 2, karena dihitung mulai dari 0
      //nama depan = 0
      //nama belakang = 1
      //tanggal terdaftar =2
      var evalDate= parseDateValue(aData[1]);
      if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
      ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
      ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
      ( dateStart <= evalDate && evalDate <= dateEnd ) )
      {
        return true;
      }
      return false;
    });

    // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
    function parseDateValue(rawDate) {
      var dateArray= rawDate.split("-");
      var parsedDate= new Date(dateArray[0], parseInt(dateArray[1])-1, dateArray[2]);  // -1 because months are from 0 to 11
      return parsedDate;
    }


    $( document ).ready(function() {
      var numberRenderer = $.fn.dataTable.render.number( ',', '.', ).display;
      //konfigurasi DataTable pada tabel dengan id example dan menambahkan  div class dateseacrhbox dengan dom untuk meletakkan inputan daterangepicker
      var $dTable = $('#example1').DataTable({
        "dom": "Bfrtip" +
        "<'row'<'col-sm-4'><'col-sm-5' <'datesearchbox1'>><'col-sm-3'>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>" ,
        "paging": false,
        "autoWidth": true,
        "buttons": ["print"],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
            i.replace(/[^\d]/g, '')*1 :
            typeof i === 'number' ?
            i : 0;
          };

          // Total over all pages
          total = api
          .column( 3 )
          .data()
          .reduce( function (a, b) {
            return intVal(a) + intVal(b);
          }, 0 );

          // Total over this page
          pageTotal = api
          .column( 3, { page: 'current'} )
          .data()
          .reduce( function (a, b) {
            return intVal(a) + intVal(b);
          }, 0 );


          // Total filtered rows on the selected column (code part added)
          var sumCol4Filtered = display.map(el => data[el][3]).reduce((a, b) => intVal(a) + intVal(b), 0 );

          // Update footer
          $( api.column( 3 ).footer() ).html(
            'Rp. '+numberRenderer( pageTotal )
          );
        }
      });

      //menambahkan daterangepicker di dalam datatables
      $("div.datesearchbox1").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch1" placeholder="Search by date range.."> </div>');

      document.getElementsByClassName("datesearchbox1")[0].style.textAlign = "right";

      //konfigurasi daterangepicker pada input dengan id datesearch
      $('#datesearch1').daterangepicker({
        autoUpdateInput: false,
        "autoApply": true,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        locale: {
          cancelLabel: 'Clear',}
        });

        //menangani proses saat apply date range
        $('#datesearch1').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
          start_date=picker.startDate.format('YYYY-MM-DD');
          end_date=picker.endDate.format('YYYY-MM-DD');
          $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
          $dTable.draw();
        });

        $('#datesearch1').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
          start_date='';
          end_date='';
          $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
          $dTable.draw();
        });

      });
      </script>

      <script type="text/javascript">

      //fungsi untuk filtering data berdasarkan tanggal
      var start_date;
      var end_date;
      var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
        var dateStart = parseDateValue(start_date);
        var dateEnd = parseDateValue(end_date);
        //Kolom tanggal yang akan kita gunakan berada dalam urutan 2, karena dihitung mulai dari 0
        //nama depan = 0
        //nama belakang = 1
        //tanggal terdaftar =2
        var evalDate= parseDateValue(aData[1]);
        if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
        ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
        ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
        ( dateStart <= evalDate && evalDate <= dateEnd ) )
        {
          return true;
        }
        return false;
      });

      // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
      function parseDateValue(rawDate) {
        var dateArray= rawDate.split("-");
        var parsedDate= new Date(dateArray[0], parseInt(dateArray[1])-1, dateArray[2]);  // -1 because months are from 0 to 11
        return parsedDate;
      }

      $(document).ready(function() {
              var numberRenderer = $.fn.dataTable.render.number( ',', '.', ).display;
          var $dTable = $('#example2').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
              var api = this.api(), data;

              // Remove the formatting to get integer data for summation
              var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[^\d]/g, '')*1 :
                typeof i === 'number' ?
                i : 0;
              };

              // Total over all pages
              total = api
              .column( 5 )
              .data()
              .reduce( function (a, b) {
                return intVal(a) + intVal(b);
              }, 0 );

              // Total over this page
              pageTotal = api
              .column( 5, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                return intVal(a) + intVal(b);
              }, 0 );


              // Total filtered rows on the selected column (code part added)
              var sumCol4Filtered = display.map(el => data[el][5]).reduce((a, b) => intVal(a) + intVal(b), 0 );

              // Update footer
              $( api.column( 5 ).footer() ).html(
                'Rp. '+numberRenderer( pageTotal )
              );
            }
          } );
      } );
      </script>


    </body>

    </html>
