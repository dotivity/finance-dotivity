<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          DATA KEUANGAN
        </h2>
        <ul class="header-dropdown m-r--5">
          <li class="dropdown">
            <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_keuangan'>Tambahkan Data</a>
          </li>
        </ul>
      </div>
      <div class="body">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
          <li role="presentation" class="active"><a href="#masuk" data-toggle="tab">PEMASUKAN</a></li>
          <li role="presentation"><a href="#keluar" data-toggle="tab">PENGELUARAN</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane animated flipInX active" id="masuk">
            <table id="example" class="table table-bordered table-striped table-hover dataTable">
              <thead>
                <tr>
                  <th>Marketplace</th>
                  <th>Tanggal</th>
                  <th>Invoice</th>
                  <th>Nama</th>
                  <th>Barang</th>
                  <th>Jumlah</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                <?php
                $no = 1;
                foreach ($masuk as $row){
                  $tanggal = $row['tgl'];
                  $marketplace = $row['marketplace'];
                  $invoice = $row['invoice'];
                  $nama = $row['nama'];
                  $barang = $row['keperluan'];
                  $angka = $row['jumlah'];
                  $rupiah = 'Rp. ' . number_format($angka);
                  echo "<tr>
                  <td>$marketplace</td>
                  <td>$tanggal</td>
                  <td>$invoice</td>
                  <td>$nama</td>
                  <td>$barang</td>
                  <td>$rupiah</td>
                  <td><center>
                  <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_keuangan/$row[id_keuangan]'><span class='glyphicon glyphicon-edit'></span></a>
                  <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_keuangan/$row[id_keuangan]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                  </center></td>
                  </tr>";
                  $no++;
                }
                ?>
              </tbody>
              <tfoot>
                <tr>
                  <th colspan='5'>Total</th>
                  <th colspan='2'></th>
                </tr>
              </tfoot>
            </table>
          </div>
          <div role="tabpanel" class="tab-pane animated flipInX" id="keluar">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Tanggal</th>
                  <th>Keperluan</th>
                  <th>Jumlah</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <th colspan='3'>Total</th>
                <th colspan='2'></th>
              </tfoot>
              <tbody>
                <?php
                $no = 1;
                foreach ($keluar as $row1){
                  $tanggal = $row1['tgl'];
                  $angka = $row1['jumlah'];
                  $uang = 'Rp. ' . number_format($angka);

                  echo "<tr>
                  <td>$row1[nama]</td>
                  <td>$tanggal</td>
                  <td>$row1[keperluan]</td>
                  <td>$uang</td>
                  <td><center>
                  <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_keuangan/$row1[id_keuangan]'><span class='glyphicon glyphicon-edit'></span></a>
                  <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_keuangan/$row1[id_keuangan]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                  </center></td>
                  </tr>";
                  $no++;
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
        <center><h1 class="card-inside-title">
          SISA SALDO SAAT INI
          <small>Pemasukan - Pengeluaran</small>
          <p>
            <?php
            $no = 1;
            foreach ($keluar as $row1){
              $tanggal = $row1['tgl'];
              $angka = $row1['jumlah'];
              $uang = 'Rp. ' . number_format($angka);

              echo "<tr>
              <td>$uang</td>

              </tr>";
              $no++;
            }
            ?> - <?php
            $query = $this->db->query("SELECT ROUND ( SUM(IF(status = 'Keluar', jumlah, 0)) ) AS subtotal2 FROM keuangan");

            foreach ($query->result_array() as $rows) {
              $dwet = $rows['subtotal2'];
              $arto = number_format($dwet,2,",",".");
              echo "
              Rp. $arto</center>";
            }
            ?>
          </p>
        </h1></center>
        <div class="demo-single-button-dropdowns">
          <?php
          $query = $this->db->query("SELECT ROUND ( SUM(IF(status = 'Masuk', jumlah, 0))-(SUM(IF( status = 'Keluar', jumlah, 0))) ) AS subtotal FROM keuangan");

          foreach ($query->result_array() as $rows) {
            $dwet = $rows['subtotal'];
            $arto = number_format($dwet,2,",",".");
            echo "
            <center><h2>Rp. $arto</h2></center>";
          }
          ?>
        </div>

        <div style="width: 800px;margin: 0px auto;">
<canvas id="myChart"></canvas>
</div>
      </div>
    </div>
  </div>
</div>
</div>
