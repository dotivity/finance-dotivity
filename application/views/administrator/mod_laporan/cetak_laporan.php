<html>
<head>
<title><?php echo $title; ?></title>
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo.png">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

  <!-- Waves Effect Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

  <!-- Animation Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

  <!-- JQuery DataTable Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

  <!-- Morris Chart Css-->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/morrisjs/morris.css" rel="stylesheet" />

  <!-- Bootstrap Material Datetime Picker Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" />
  <!-- Wait Me Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/waitme/waitMe.css" rel="stylesheet" />

  <!-- Bootstrap Select Css -->
  <link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

  <!-- Custom Css -->
  <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

  <!-- AdminBSB Themes. You can choose a theme from css/ themes instead of get all themes -->
  <link href="<?php echo base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />
</head>
<body onload="window.print()">
<div class="col-xs-12">
  <div style="text-align:justify; margin-top: 20px">
    <img src="<?php echo base_url(); ?>assets/images/logo_sragen.jpg" style="width: 78px; height: 80px; float:left; margin:0 8px 4px 0;"/>
    <p style="text-align: center; line-height: 20px">
      <span style="font-size: 15px">PEMERINTAH KABUPATEN SRAGEN</span><br/>
      <span style="font-size: 20px;"><strong>RSUD dr. SOEHADI PRIJONEGORO</strong></span><br/>
      <span style="font-size: 12px">Jln. Raya Sukowati No. 534 Telp. (271) 891068 Sragen 57272</span><br/>
      <span style="font-size: 12px">Website : www.rssp.sragenkab.go.id dan Email : rsudsragen1958@gmail.com</span>
    </p>
  </div>
  <div style="clear:both"></div><br/>
  <hr style="border: 2px groove #000000;margin-top: -2px; width:100%"/>
  <hr style="border: 1px groove #000000; margin-top: -19px; width:100%"/>
</div>
<div class="col-xs-12">
  <h3>PEMASUKAN KEUANGAN</h3>
  <table class="table table-bordered table-striped">
    <thead>
        <tr>
          <th>Marketplace</th>
          <th>Tanggal</th>
          <th>Invoice</th>
          <th>Nama</th>
          <th>Barang</th>
          <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      foreach ($tampil_data->result_array() as $rows) {
        $tanggal01 = tgl_indo($rows['tgl']);
        $angka = $rows['jumlah'];
        $rupiah = 'Rp. ' . number_format($angka,2,",",".");
        ?>
        <tr>
          <th scope="row"><?php echo $rows['marketplace'] ?></th>
          <td><?php echo $tanggal01 ?></td>
          <td><?php echo $rows['invoice'] ?></td>
          <td><?php echo $rows['nama'] ?></td>
          <td><?php echo $rows['keperluan'] ?></td>
          <td><?php echo $rupiah ?></td>
        </tr>
        <?php $no++; } ?>
      </tbody>
      <tfoot>
        <?php
          $mlebu = $this->db->query("SELECT status , SUM(jumlah) AS masuk FROM keuangan WHERE status = 'Masuk'")->result_array();
          foreach ($mlebu as $anu) {
            $a = $anu['masuk'];
            $b = number_format($a,2,",",".");
            echo "<tr>
              <th colspan='5'>Jumlah</th>
              <th colspan='1'>Rp. $b</th>
            </tr>";
          }
        ?>
      </tfoot>
  </table>

  <h3>PENGELUARAN KEUANGAN</h3>
  <table class="table table-bordered table-striped">
      <thead>
          <tr>
            <th>Tanggal</th>
            <th>Nama</th>
            <th>Keperluan</th>
            <th>Jumlah</th>
          </tr>
      </thead>
      <tbody>
         <?php
            $no = 1;
            foreach ($tampil_data1->result_array() as $rows1) {
              $tanggal = tgl_indo($rows1['tgl']);
          ?>
          <tr>
            <th scope="row"><?php echo $tanggal ?></th>
            <td><?php echo $rows1['nama'] ?></td>
            <td><?php echo $rows1['keperluan'] ?></td>
            <td><?php echo $rows1['jumlah'] ?></td>
          </tr>
          <?php $no++; } ?>
      </tbody>
  </table>
</div>

<!-- Jquery Core Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery/jquery-3.3.1.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- Select Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/morrisjs/morris.js"></script>

<!-- ChartJs -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/chartjs/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/flot-charts/jquery.flot.time.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<!-- Autosize Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/autosize/autosize.js"></script>

<!-- Moment Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/momentjs/moment.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src=" https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src=" https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<!-- Custom Js -->
<script src="<?php echo base_url(); ?>assets/admin/js/admin.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/index.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/tables/jquery-datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/pages/forms/basic-form-elements.js"></script>

<!-- Demo Js -->
<script src="<?php echo base_url(); ?>assets/admin/js/demo.js"></script>
<script type="text/javascript">
function jam(){
  var waktu = new Date();
  var jam = waktu.getHours();
  var menit = waktu.getMinutes();
  var detik = waktu.getSeconds();

  if (jam < 10){ jam = "0" + jam; }
  if (menit < 10){ menit = "0" + menit; }
  if (detik < 10){ detik = "0" + detik; }
  var jam_div = document.getElementById('jam');
  jam_div.innerHTML = jam + ":" + menit + ":" + detik;
  setTimeout("jam()", 1000);
} jam();
</script>

<script>
$(function(){
  $("#tampil").click(function(){
    var vtanggal = $("#vtanggal").val();
    $.ajax({
      url:"<?php echo site_url('administrator/tampil_data');?>",
      type:"POST",
      data:"vtanggal="+vtanggal,
      cache:false,
      success:function(html){
        $("#tampil_data").html(html);
      }
    })
  })

})
</script>

<script>
$('#date').bootstrapMaterialDatePicker({
  time: false,
  clearButton: true
});
</script>

<script type="text/javascript">
//fungsi untuk filtering data berdasarkan tanggal
var start_date;
var end_date;
var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
  var dateStart = parseDateValue(start_date);
  var dateEnd = parseDateValue(end_date);
  //Kolom tanggal yang akan kita gunakan berada dalam urutan 2, karena dihitung mulai dari 0
  //nama depan = 0
  //nama belakang = 1
  //tanggal terdaftar =2
  var evalDate= parseDateValue(aData[1]);
  if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
  ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
  ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
  ( dateStart <= evalDate && evalDate <= dateEnd ) )
  {
    return true;
  }
  return false;
});

// fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
function parseDateValue(rawDate) {
  var dateArray= rawDate.split("-");
  var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11
  return parsedDate;
}

$( document ).ready(function() {
   var numberRenderer = $.fn.dataTable.render.number( ',', '.', ).display;
  //konfigurasi DataTable pada tabel dengan id example dan menambahkan  div class dateseacrhbox dengan dom untuk meletakkan inputan daterangepicker
  var $dTable = $('#example').DataTable({
    "dom": "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    "paging": false,
    "autoWidth": true,
    "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[^\d]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };

          // Total over all pages
          total = api
              .column( 4 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

          // Total over this page
          pageTotal = api
              .column( 4, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );


          // Total filtered rows on the selected column (code part added)
          var sumCol4Filtered = display.map(el => data[el][4]).reduce((a, b) => intVal(a) + intVal(b), 0 );

          // Update footer
          $( api.column( 4 ).footer() ).html(
              'Rp. '+numberRenderer( pageTotal ) +' ( Rp. '+numberRenderer( total ) +' total))'
          );
      }
  });

  //menambahkan daterangepicker di dalam datatables
  $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range.."> </div>');

  document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";

  //konfigurasi daterangepicker pada input dengan id datesearch
  $('#datesearch').daterangepicker({
    autoUpdateInput: false
  });

  //menangani proses saat apply date range
  $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    start_date=picker.startDate.format('YYYY-MM-DD');
    end_date=picker.endDate.format('YYYY-MM-DD');
    $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
    $dTable.draw();
  });

  $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    start_date='';
    end_date='';
    $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
    $dTable.draw();
  });

});
</script>
</body>
</html>
