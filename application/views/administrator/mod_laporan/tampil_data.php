
<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
  <div class="card">
    <div class="header">
      <h2>
        PEMASUKAN
      </h2>
    </div>
    <div class="body table-responsive">
      <table id="example2" class="table table-bordered table-striped table-hover dataTable">
        <thead>
          <tr>
            <th>Marketplace</th>
            <th>Tanggal</th>
            <th>Invoice</th>
            <th>Nama</th>
            <th>Barang</th>
            <th>Jumlah</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          foreach ($tampil_data->result_array() as $rows) {
            $tanggal1 = tgl_indo($rows['tgl']);
            $angka = $rows['jumlah'];
            $rupiah = 'Rp. ' . number_format($angka,2,",",".");
            ?>
            <tr>
              <th scope="row"><?php echo $rows['marketplace'] ?></th>
              <td><?php echo $tanggal1 ?></td>
              <td><?php echo $rows['invoice'] ?></td>
              <td><?php echo $rows['nama'] ?></td>
              <td><?php echo $rows['keperluan'] ?></td>
              <td><?php echo $rupiah ?></td>
            </tr>
            <?php $no++; } ?>
          </tbody>
          <tfoot>
            <tr>
              <th scope="row">total</th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          PENGELUARAN
        </h2>
      </div>
      <div class="body table-responsive">
        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
          <thead>
            <tr>
              <th>Tanggal</th>
              <th>Nama</th>
              <th>Keperluan</th>
              <th>Jumlah</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($tampil_data1->result_array() as $rows1) {
              $tanggal = tgl_indo($rows1['tgl']);
              $angka = $rows1['jumlah'];
              $rupiah1 = 'Rp. ' . number_format($angka,2,",",".");
              ?>
              <tr>
                <th scope="row"><?php echo $tanggal ?></th>
                <td><?php echo $rows1['nama'] ?></td>
                <td><?php echo $rows1['keperluan'] ?></td>
                <td><?php echo $rupiah1 ?></td>
              </tr>
              <?php $no++; } ?>
            </tbody>
            <tfoot>
              <?php
                $metu = $this->db->query("SELECT status , SUM(jumlah) AS keluar FROM keuangan WHERE status = 'keluar'")->result_array();
                foreach ($metu as $anu1) {
                  $a1 = $anu1['keluar'];
                  $b1 = number_format($a1,2,",",".");
                  echo "<tr>
                    <th colspan='3'>Jumlah</th>
                    <th colspan='1'>Rp. $b1</th>
                  </tr>";
                }
              ?>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
